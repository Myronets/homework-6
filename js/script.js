// Задание
// Добавить в домашнее задание HTML/CSS №6 (Flex/Grid) различные эффекты с использованием jQuery
//
// Технические требования:
//
//     Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
//     При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
//     Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка
//     "Наверх" с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
//     Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle()
//     (прятать и показывать по клику) для данной секции.


$('a[href^="#"]').click(function(){
    let target = $(this).attr('href');
    $('html').animate({scrollTop: $(target).offset().top}, 800);
    return false;
});


$(function() {
    $(window).scroll(function() {
        $(this).scrollTop() > innerHeight ? $('.toTop').fadeIn() : $('.toTop').fadeOut()
    });

    $('.toTop').click(function() {
        $('html').animate({scrollTop:0},800);
    });
});


let $btn = $('<button>hide</button>');
$btn.addClass('btn-news');
$('.news').after($btn);

$btn.click(function () {
    $('.news-items-wrapper').slideToggle("slow");
    $btn.text() === 'hide' ? $btn.text('show') : $btn.text('hide')
});

